import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule, MatIconModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { MatGridListModule } from '@angular/material/grid-list';
import { MainViewComponent } from './main-view/main-view.component';
import { KnightListComponent } from './knight-list/knight-list.component';
import { DragonsListComponent } from './dragons-list/dragons-list.component';
import { PresentationalComponent } from './presentational/presentational.component';
import { FightBoardComponent } from './fight-board/fight-board.component';
import { FigtherCredentialComponent } from './figther-credential/figther-credential.component';
import { PresentionalFighterComponent } from './presentional-fighter/presentional-fighter.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';

@NgModule({
  declarations: [
    AppComponent,
    MainViewComponent,
    KnightListComponent,
    DragonsListComponent,
    PresentationalComponent,
    FightBoardComponent,
    FigtherCredentialComponent,
    PresentionalFighterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatGridListModule,
    MatProgressBarModule,
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
