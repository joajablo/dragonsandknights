import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Dragon } from '../dragon';

@Component({
  selector: 'app-dragons-list',
  templateUrl: './dragons-list.component.html',
  styleUrls: ['./dragons-list.component.css']
})
export class DragonsListComponent implements OnInit {

  @Input()
  dragons: Dragon[];

  @Output()
  selectedDragon: EventEmitter<Dragon> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  selectDragon(dragon: Dragon) {
    this.selectedDragon.emit(dragon);
  }

}
