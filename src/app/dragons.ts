import { Dragon } from './dragon';

export const DRAGONS: Dragon[] = [
    new Dragon(1000, 'Psysiu1', 20, 10, 50, '../assets/images/dragon.jpg'),
    new Dragon(2000, 'Psysiu2', 40, 30, 60, '../assets/images/dragon.jpg'),
    new Dragon(1400, 'Psysiu3', 60, 20, 80, '../assets/images/dragon.jpg')
];
