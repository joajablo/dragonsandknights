import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-fight-board',
  templateUrl: './fight-board.component.html',
  styleUrls: ['./fight-board.component.css']
})
export class FightBoardComponent implements OnInit {

  constructor() { }

  @Input()
  message: string;

  ngOnInit() {
  }

}
