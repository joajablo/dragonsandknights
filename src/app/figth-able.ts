export interface FigthAble {
    hp: number;
    name: string;
    atk: number;
    def: number;
    acc: number;
    imgurl: string;
}
