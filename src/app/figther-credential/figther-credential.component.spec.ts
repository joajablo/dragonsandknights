import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FigtherCredentialComponent } from './figther-credential.component';

describe('FigtherCredentialComponent', () => {
  let component: FigtherCredentialComponent;
  let fixture: ComponentFixture<FigtherCredentialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FigtherCredentialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FigtherCredentialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
