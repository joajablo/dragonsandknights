import { Component, OnInit, Input } from '@angular/core';
import {Figther} from '../figther';

@Component({
  selector: 'app-figther-credential',
  templateUrl: './figther-credential.component.html',
  styleUrls: ['./figther-credential.component.css']
})
export class FigtherCredentialComponent implements OnInit {

  constructor() { }

  @Input()
  figther: Figther;

  ngOnInit() {
  }

}
