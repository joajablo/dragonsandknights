import { FigthAble } from './figth-able';

export abstract class Figther implements FigthAble {

    public currentHp: number;
    private _hp: number;

    get hp(): number {
        return this._hp;
    }

    set hp(value: number) {
        this._hp = value;
        this.currentHp = value;
    }

    get hpPercentage(): number {
        return (this.currentHp / this._hp) * 100;
    }

    constructor(hp: number, public name: string, public atk: number, public def: number, public acc: number, public imgurl: string) {

    }

    private randomInt(min: number, max: number) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    public hit(target: Figther): string {

        const k100 = this.randomInt(1, 100);
        // const hit = false;

        if (target.acc < k100) {
            return this.attack(target);
        }
        return `Missed.\n`;
    }

    private attack(target: Figther): string {

        let result = ``;
        const k20 = this.randomInt(1, 20);

        let damage = this.atk + k20;
        if (k20 < 4) {
            this.hp -= damage;

        } else {
            if (k20 > 17) {
                damage *= 5;
                result += `${this.name} hits critical with ${damage - target.def} force. \n`;
            }

            if (damage > target.def) {

                target.currentHp = target.currentHp - (damage - target.def);
                result += `${this.name} hits ${target.name} with ${damage} force. \n`;
            }
        }

        return result;


    }
}
