import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Knight } from '../knight';

@Component({
  selector: 'app-knight-list',
  templateUrl: './knight-list.component.html',
  styleUrls: ['./knight-list.component.css']
})
export class KnightListComponent implements OnInit {

  @Input()
  knights: Knight[];

  @Output()
  selectedKnight: EventEmitter<Knight> = new EventEmitter();

  constructor() { }

  ngOnInit() {
    console.log(this.knights);
  }

  selectKnight(knight: Knight) {
    this.selectedKnight.emit(knight);
  }
}
