import { Figther } from './figther';

export class Knight extends Figther {

    constructor(public hp: number, public name: string, public atk: number, public def: number, public acc: number, public imgurl: string) {
        super(hp, name, atk, def, acc, imgurl);
    }

}
