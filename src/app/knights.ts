import { Knight } from './knight';

export const KNIGHTS: Knight[] = [
    new Knight(350, 'Przemek1', 100, 5, 80, '../assets/images/knight.jpg'),
    new Knight(360, 'Przemek2', 140, 3, 90, '../assets/images/knight.jpg'),
    new Knight(550, 'Przemek3', 200, 10, 20, '../assets/images/knight.jpg')
];
