import { Component, OnInit } from '@angular/core';
import { Dragon } from '../dragon';
import { Knight } from '../knight';
import { KNIGHTS } from '../knights';
import { DRAGONS } from '../dragons';

@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.css']
})
export class MainViewComponent implements OnInit {

  selectedKnight: Knight;
  selectedDragon: Dragon;
  knights: Knight[];
  dragons: Dragon[];
  message: string;

  constructor() { }

  ngOnInit() {
    this.knights = KNIGHTS;
    this.dragons = DRAGONS;
  }

  onSelectKnight(knigth: Knight) {
    this.selectedKnight = knigth;
  }

  onSelectDragon(dragon: Dragon) {
    this.selectedDragon = dragon;
  }

  initialFight() {

    this.message = ``;
    while (true) {

        if (this.selectedKnight.currentHp > 0) {
          this.message += this.selectedKnight.hit(this.selectedDragon);
        } else {
          this.message += `${this.selectedDragon.name} is the winner!` + `\n`;
            break;
        }

        if (this.selectedDragon.currentHp > 0) {
          this.message += this.selectedDragon.hit(this.selectedKnight);
        } else {
          this.message += `${this.selectedKnight.name} is the winner!` + `\n`;
          break;
        }
    }
  }
}
