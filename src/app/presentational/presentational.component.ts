import { Component, OnInit, Input } from '@angular/core';
import { Dragon } from '../dragon';
import { Knight } from '../knight';

@Component({
  selector: 'app-presentational',
  templateUrl: './presentational.component.html',
  styleUrls: ['./presentational.component.css']
})
export class PresentationalComponent implements OnInit {

  @Input()
  dragon: Dragon;

  @Input()
  knight: Knight;

  constructor() { }

  ngOnInit() {
  }

}
