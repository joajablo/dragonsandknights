import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PresentionalFighterComponent } from './presentional-fighter.component';

describe('PresentionalFighterComponent', () => {
  let component: PresentionalFighterComponent;
  let fixture: ComponentFixture<PresentionalFighterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresentionalFighterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresentionalFighterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
