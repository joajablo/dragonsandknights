import { Component, OnInit, Input } from '@angular/core';
import { Figther } from '../figther';

@Component({
  selector: 'app-presentional-fighter',
  templateUrl: './presentional-fighter.component.html',
  styleUrls: ['./presentional-fighter.component.css']
})
export class PresentionalFighterComponent implements OnInit {

  @Input()
  fighter: Figther;

  @Input()
  leftPanel: boolean;

  constructor() { }

  ngOnInit() {
  }

}
